# Wordpress Snippet
==================

This is a collection of WordPress snippets and autocompletions for Visual Studio Code

- License: GNU/GPL
- Version: 0.0.1
- Author URI: http://tungvn.info
- Extension URI: https://gitlab.com/tungvn/wordpress-snippet

### Features

Autocomplete for:

    WP version : 3.7.0

    Functions          : 1734
    Constants/Classes  :  191
    
### Install instructions

Install via Extension Marketplace
- Open Command on Visual Studio Code (Ctrl+Shift+P on Windows or Cmd+Shift+P on Mac/OSX)
- > ext install wordpress-snippet
- Wait until install complete and restart VS Code

Install by Packaged Extension (.vsix)
- You can manually install an VS Code extension packaged in a .vsix file. Simply install using the VS Code command line providing the path to the .vsix file.
- >code extension_name.vsix
- The extension will be installed under your user .vscode/extensions folder. You may provide multiple .vsix files on the command line to install multiple extensions at once.
- You can also install a .vsix by opening the file from within VS Code. Run File > Open File... or Ctrl+O and select the extension .vsix.

### Changelogs
Version 0.8: First commit. Update functions, constant and classes from Wordpress 3.7

### On my way
- Update to WP lastest version (current: 4.5)
- Update description for funtions, constant and classes. 

### Special thanks
- [purplefish32](https://github.com/purplefish32) with https://github.com/purplefish32/sublime-text-2-wordpress